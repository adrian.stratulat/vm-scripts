#!/bin/bash

vboxmanage modifyvm  'cpl-vm-lite' --uart1 off
vboxmanage storageattach 'cpl-vm-lite' --storagectl SCSI --port 1 --medium none
vboxmanage sharedfolder remove 'cpl-vm-lite' --name vagrant
vboxmanage export 'cpl-vm-lite' --ovf10 -o cpl-vm-lite-1.5.ova
