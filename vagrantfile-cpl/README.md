After the machine is provisioned, the following steps need to be
taken manually:

In firefox:
* Set the homepage to curs.upb.ro.
* Install AdBlockPlus (adblockplus.org), disable nonintrusive advertising, pick RoList.
* Install Privacy Badger (privacybadger.org).

In a console:
```bash
sudo updatedb
```
