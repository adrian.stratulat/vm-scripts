#!/bin/bash

vboxmanage modifyvm  'cpl-vm' --uart1 off
vboxmanage storageattach 'cpl-vm' --storagectl SCSI --port 1 --medium none
vboxmanage sharedfolder remove 'cpl-vm' --name vagrant
vboxmanage export 'cpl-vm' --ovf10 -o cpl-vm-1.5.ova
