#!/bin/bash

# http://www.nongnu.org/avr-libc/user-manual/install_tools.html
# Adapted from http://blog.zakkemble.co.uk/avr-gcc-builds/

# It downloads, builds on the local machine, and packages as deb files
# the toolkit.

# How to run:
# - chmod +x build_debs.sh 
# - time sudo ./build_debs.sh

JOBCOUNT=8

# Stop on errors
set -e

OPTS_BINUTILS='
	--prefix=/usr
	--target=avr
	--disable-nls
'

OPTS_GCC_BARE='
	--prefix=/usr
	--target=avr
	--enable-languages=c,c++
	--disable-nls
	--disable-libssp
	--disable-libada
	--with-dwarf2
	--disable-shared
	--enable-static
'

OPTS_LIBC='
	--prefix=/usr
'

apt-get install -y checkinstall texinfo

CC=""
export CC

echo "Downloading sources..."
wget ftp://ftp.mirrorservice.org/sites/ftp.gnu.org/gnu/binutils/binutils-2.34.tar.xz
wget ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-8.2.0/gcc-8.2.0.tar.xz
wget ftp://ftp.mirrorservice.org/sites/download.savannah.gnu.org/releases/avr-libc/avr-libc-2.0.0.tar.bz2

# Make AVR-Binutils
echo "Making Binutils..."
echo "Extracting..."
tar xf binutils-2.34.tar.xz
mkdir -p binutils-2.34/build-avr
cd binutils-2.34/build-avr
../configure $OPTS_BINUTILS
make -j $JOBCOUNT
checkinstall --pkgname=binutils-avr --pkgversion="1:2.34+PM" --provides=binutils-avr --exclude=/usr/local/share/info --default make install-strip
cd ../../

# Make AVR-GCC-BARE
echo "Making GCC-BARE..."
echo "Extracting..."
tar xf gcc-8.2.0.tar.xz
mkdir -p gcc-8.2.0/build-avr-bare
cd gcc-8.2.0
./contrib/download_prerequisites
cd build-avr-bare
../configure $OPTS_GCC_BARE
make -j $JOBCOUNT
checkinstall --pkgname=gcc-avr-bare --pkgversion="1:8.2.0+PM" --provides=gcc-avr --exclude=/home --default make install-strip
cd ../../

# Make AVR-LibC
echo "Making AVR-LibC..."
echo "Extracting..."
bunzip2 -c avr-libc-2.0.0.tar.bz2 | tar xf -
mkdir -p avr-libc-2.0.0/build-avr
cd avr-libc-2.0.0/build-avr
../configure $OPTS_LIBC --host=avr --build=`../config.guess`
make -j $JOBCOUNT
checkinstall --pkgname=avr-libc --pkgversion="1:2.0.0+PM" --provides=avr-libc --default make install-strip
cd ../../

# Make AVR-GCC-FULL
echo "Making GCC-FULL..."

rm -rf gcc-8.2.0

echo "Extracting..."
tar xf gcc-8.2.0.tar.xz
mkdir -p gcc-8.2.0/build-avr-full
cd gcc-8.2.0

#
# Configure.ac tests by default if the target gcc can run code with dlopen().
# Because avr-libc does not implement dynamic loading, this test usually fails.
# Do not perform this test.
#
patch -p0  < ../configure.patch

./contrib/download_prerequisites
cd build-avr-full

../configure \
	--prefix=/usr \
	--target=avr \
	--enable-languages=c,c++ \
	--disable-nls \
	--disable-libssp \
	--disable-libada \
	--with-dwarf2 \
	--disable-shared \
	--enable-static \
	--with-avrlibc \
	--enable-libstdcxx \
	--enable-cxx-flags='-fno-exceptions -fno-rtti' \
	--disable-libstdcxx-verbose \
	--disable-vtable-verify \
	--disable-libstdcxx-time \
	--disable-libstdcxx-threads \
	--disable-threads \
	--enable-libstdcxx-allocator=malloc \
	--disable-libstdcxx-filesystem-ts

make -j $JOBCOUNT

apt-get remove -y gcc-avr-bare

checkinstall --pkgname=gcc-avr-full --pkgversion="1:8.2.0+PM" --provides=gcc-avr --exclude=/home --default make install-strip
cd ../../


echo "Done."

exit 0

