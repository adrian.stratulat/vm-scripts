#!/bin/bash

vboxmanage modifyvm  'pp-vm' --uart1 off
vboxmanage storageattach 'pp-vm' --storagectl SCSI --port 1 --medium none
vboxmanage sharedfolder remove 'pp-vm' --name vagrant
vboxmanage export 'pp-vm' --ovf10 -o pp-vm-1.1.ova
