#!/bin/bash

vboxmanage storageattach 'isrm-vm' --storagectl SCSI --port 1 --medium none
vboxmanage sharedfolder remove 'isrm-vm' --name vagrant
vboxmanage export 'isrm-vm' --ovf10 -o isrm-vm-1.2.ova
