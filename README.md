# VM creation tutorial

## Setting up the environment

Tested on Linux Mint 19 (codename Tara, based on Ubuntu 18.04).
Any reasonable recent Linux distribution should work.

```
    $ sudo apt-get install vagrant virtualbox virtualbox-qt git-lfs python-pip python-setuptools
    $ pip install ansible
    $ vagrant plugin install vagrant-reload
```

## Building a VM

```
    $ git clone git@gitlab.com:adrian.stratulat/vm-scripts.git
    $ cd vagrantfile-pm
    $ vagrant up
```

Running the `vagrant up` command will create and configure the VM.

## Common errors

* "/usr/bin/python: not found"

If you see something like this...

```
fatal: [default]: FAILED! => {"changed": false, "module_stderr": "Shared connection to 127.0.0.1 closed.\r\n", "module_stdout": "/bin/sh: 1: /usr/bin/python: not found\r\n", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 127}
```
... it's probably because you don't have a recent enough version of "ansible".

Run...
```bash
$ pip install -U ansible
```
...and try again
