# Physical machine install guide

Before running the install steps, make sure you have the entry in the partition table.

```
sudo dd if=pm-vm-physical-1.0.ext4 of=/dev/sda2 bs=4M status=progress
sudo resize2fs /dev/sda2
sudo e2fsck -fp /dev/sda2
sudo mount /dev/sda2 /mnt
sudo grub-install --root-directory=/dev/mnt /dev/sda
```

At first, the installer will only be aware of the OS you installed it with.
To auto-detect the other installed operating systems and update the bootloader entry,
run the following command with the unfrozen Linux install:
 
```
sudo update-grub
```
