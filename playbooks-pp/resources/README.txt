Mașina virtuală pentru Paradigme de Programare.

===============================================================================

Dacă funcția de Copy-Paste nu merge corect între guest și host pe VirtualBox,
încercați următorii pași:

- Cu mașina virtuală pornită, intrați in meniul "Devices" al Virtualbox și apoi
"Insert Guest Additions CD image...".

- Într-un terminal al mașinii virtuale dați următoarele comenzi:

mkdir test
sudo mount -o ro /dev/sr0 ./test
cd ./test
sudo ./VBoxLinuxAdditions.run
reboot

